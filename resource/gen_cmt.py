import cv2
import numpy as np
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
import os
import random

CHCNVN = [226, 30, 600, 30, 600, 50, 226, 50, 'CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM']
DL = [292, 50, 360, 50, 360, 72, 295, 72, 'Độc lập']
TD = [373, 51, 426, 51, 426, 72, 373, 72, 'Tự do']
HP = [435, 51, 531, 51, 531, 72, 438, 72, 'Hạnh phúc']
GCMNN = [235, 75, 595, 75, 595, 105, 273, 105, 'GIẤY CHỨNG MINH NHÂN DÂN']
SO = [268, 110, 305, 110, 305, 138, 268, 138, 'SỐ']
HT = [205, 160, 267, 160, 267, 180, 205, 180, 'Họ tên']
NS = [207, 225, 300, 225, 300, 248, 207, 248, 'Ngày sinh']
NQ = [207, 258, 330, 258, 330, 282, 270, 282, 'Nguyên quán']
DKTT = [207, 325, 400, 325, 400, 350, 207, 350, 'Nơi DKHK thường trú']


def add_point(img, number_point=120):
    img = np.asarray(img)
    h, w, _ = img.shape
    x = np.random.choice(h, number_point)
    y = np.random.choice(w, number_point)
    for i in range(0, len(x)):
        img = cv2.circle(img, (y[i], x[i]), 2, (60, 50, 60), -1)
    return img


def gama_correction(img, gama=1):
    rnd = random.random()
    if rnd < 0.4:
        gama = 0.5
    elif rnd < 0.8:
        gama = 1
    else:
        gama = 2
    inv_gama = 1.0 / gama
    img = np.asarray(img)
    table = np.array([((i / 255.0) ** inv_gama) * 255 for i in np.arange(0, 256)]).astype("uint8")
    img = cv2.LUT(img, table)
    return img


def gen_number(size):
    array_number = np.random.randint(10, size=size)
    str_number = ''
    for number in array_number:
        str_number += str(number) + " "
    return str_number


def draw_number(img, length_number, font_path, font_size):
    text_number = gen_number(size=length_number)
    font = ImageFont.truetype(font_path, font_size)
    text_w, text_h = font.getsize(text_number)
    off_set_x = int(text_w / 34) + 2
    length = off_set_x * 17
    x_start = random.randint(310, 560 - length)
    draw = ImageDraw.Draw(img)
    rnd = random.random()
    y_start = 112
    x_start_org = x_start
    for number in text_number:
        if rnd < 0.5:
            fill = (90, 0, 0, 255)
        else:
            fill = (50, 60, 50, 255)
        draw.text((x_start, y_start), number, font=font, fill=fill)
        x_start += off_set_x
    return img, [x_start_org - 2, y_start - 2, x_start_org + length, y_start, x_start_org + length + 8,
                 y_start + text_h + 2,
                 x_start_org, y_start + text_h, text_number]
    # img.show()


def draw_name(img, font_path, font_size, path_names):
    path_name = os.path.join(path_names, "names_sorted_merged.txt")
    path_surname = os.path.join(path_names, "surname.txt")
    path_family = os.path.join(path_names, "family_name.txt")
    path_adress = os.path.join(path_names, "phuongxa.txt")
    list_names = read_file_names(path_name)
    list_family = read_file_names(path_family)
    list_surname = read_file_names(path_surname)
    name = random.choice(list_names)
    family = random.choice(list_family)
    surname = random.choice(list_surname)
    rnd = random.random()
    if rnd < 0.4:
        full_name = family + " " + surname + " " + name
    elif rnd < 0.7:
        family2 = random.choice(list_family)
        rnd1 = random.random()
        if rnd1 < 0.5:
            full_name = family + " " + family2 + " " + surname + " " + name
        else:
            full_name = family2 + " " + family + " " + surname + " " + name
    else:
        full_name = family + " " + name
    rnd = random.random()
    if rnd < 0.7:
        full_name = full_name.upper()
    font = ImageFont.truetype(font_path, font_size)
    text_w, text_h = font.getsize(full_name)
    draw = ImageDraw.Draw(img)
    if text_w < 610 - 275:
        y_start = 153
        length = 610 - text_w
        start_x = random.randint(275, length)
        draw.text((start_x, y_start), full_name, font=font, fill=(50, 60, 50, 255))
    else:
        y_start = 183
        while True:
            name = random.choice(list_names)
            family = random.choice(list_family)
            surname = random.choice(list_surname)
            rnd = random.random()
            if rnd < 0.4:
                full_name = family + " " + surname + " " + name
            elif rnd < 0.7:
                family2 = random.choice(list_family)
                rnd1 = random.random()
                if rnd1 < 0.5:
                    full_name = family + " " + family2 + " " + surname + " " + name
                else:
                    full_name = family2 + " " + family + " " + surname + " " + name
            else:
                full_name = family + " " + name
            text_w, text_h = font.getsize(full_name)
            if text_w < 610 - 240:
                break
        length = 610 - text_w
        start_x = random.randint(240, length)
    draw.text((start_x, y_start), full_name, font=font, fill=(50, 60, 50, 255))
    box_name = [start_x - 2, y_start - 8, start_x + text_w, y_start, start_x + text_w + 2, y_start + text_h + 2,
                start_x,
                y_start + text_h, full_name]
    img, box_number = draw_number(img, length_number=9, font_path=font_path, font_size=31)

    img, box_dob = dob(img, font_path=font_path, font_size=20)

    img, all_address = draw_address(img, font_path=font_path, font_size=18, path_adress=path_adress)

    rnd = random.random()

    img = gama_correction(img=add_point(img))
    address1 = all_address[0]
    address2 = all_address[1]
    address3 = all_address[2]
    address4 = all_address[3]
    img1 = np.array(img)
    img1 = cv2.rectangle(img1, (box_number[0], box_number[1]), (box_number[4], box_number[5]), (0, 255, 0), 1)
    img1 = cv2.rectangle(img1, (box_dob[0], box_dob[1]), (box_dob[4], box_dob[5]), (0, 255, 0), 1)
    img1 = cv2.rectangle(img1, (address1[0], address1[1]), (address1[4], address1[5]), (0, 255, 0), 1)
    img1 = cv2.rectangle(img1, (address2[0], address2[1]), (address2[4], address2[5]), (0, 255, 0), 1)
    img1 = cv2.rectangle(img1, (address3[0], address3[1]), (address3[4], address3[5]), (0, 255, 0), 1)
    img1 = cv2.rectangle(img1, (address4[0], address4[1]), (address4[4], address4[5]), (0, 255, 0), 1)
    img1 = cv2.rectangle(img1, (box_name[0], box_name[1]), (box_name[4], box_name[5]), (0, 255, 0), 1)
    img1 = cv2.rectangle(img1, (CHCNVN[0], CHCNVN[1]), (CHCNVN[4], CHCNVN[5]), (0, 255, 0), 1)
    img1 = cv2.rectangle(img1, (DL[0], DL[1]), (DL[4], DL[5]), (0, 255, 0), 1)
    img1 = cv2.rectangle(img1, (TD[0], TD[1]), (TD[4], TD[5]), (0, 255, 0), 1)
    img1 = cv2.rectangle(img1, (HP[0], HP[1]), (HP[4], HP[5]), (0, 255, 0), 1)
    img1 = cv2.rectangle(img1, (GCMNN[0], GCMNN[1]), (GCMNN[4], GCMNN[5]), (0, 255, 0), 1)
    img1 = cv2.rectangle(img1, (SO[0], SO[1]), (SO[4], SO[5]), (0, 255, 0), 1)
    img1 = cv2.rectangle(img1, (HT[0], HT[1]), (HT[4], HT[5]), (0, 255, 0), 1)
    img1 = cv2.rectangle(img1, (NS[0], NS[1]), (NS[4], NS[5]), (0, 255, 0), 1)
    img1 = cv2.rectangle(img1, (NQ[0], NQ[1]), (NQ[4], NQ[5]), (0, 255, 0), 1)
    img1 = cv2.rectangle(img1, (DKTT[0], DKTT[1]), (DKTT[4], DKTT[5]), (0, 255, 0), 1)
    img = np.array(img)
    if rnd < 0.2:
        img = cv2.GaussianBlur(img, (9, 9), 0)
    cv2.imshow("Gaussian", img1)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    return img, box_name, box_number, box_dob, all_address


def dob(img, font_path, font_size):
    days = range(1, 32)
    months = range(1, 13)
    years = range(1920, 2021)
    day = random.choice(days)
    month = random.choice(months)
    year = random.choice(years)

    if day < 10:
        rnd = random.random()
        if rnd < 0.5:
            day = "0" + str(day)
        day = str(day)
    if month < 10:
        rnd = random.random()
        if rnd < 0.5:
            month = "0" + str(month)
        month = str(month)

    rnd = random.random()
    if rnd < 0.5:
        dob = str(day) + "-" + str(month) + "-" + str(year)
    else:
        dob = str(day) + "/" + str(month) + "/" + str(year)
    font = ImageFont.truetype(font_path, font_size)
    text_w, text_h = font.getsize(dob)
    x_start = random.randint(300, 560 - text_w)
    y_start = 228
    draw = ImageDraw.Draw(img)
    draw.text((x_start, y_start), dob, font=font, fill=(50, 60, 50, 255))
    return img, [x_start - 2, y_start - 2, x_start + text_w, y_start, x_start + text_w + 4, y_start + text_h + 2,
                 x_start,
                 y_start + text_h, dob]


def read_file_names(path_file):
    list_word = []
    with open(path_file, encoding='utf-8') as fi:
        for word in fi:
            word = word.strip("\n")
            list_word.append(word)
    return list_word


def read_file_address(path_file):
    level1 = []
    level2 = []
    level3 = []
    with open(path_file, encoding='utf-8') as fi:
        for line in fi:
            line = line.strip(" ")
            list_level = line.split(",")
            level1.append(list_level[0])
            level2.append(list_level[1])
            level3.append(list_level[2])
    return level1, level2, level3


def draw_address(img, font_path, font_size, path_adress):
    level1, level2, level3 = read_file_address(path_adress)
    rndidx = random.randint(0, len(level1))
    text_level1 = level1[rndidx]
    text_level2 = level2[rndidx]
    text_level3 = level3[rndidx]
    font = ImageFont.truetype(font_path, font_size)
    text_w, text_h = font.getsize(text_level1)
    all_address = []
    y_start = 262
    if text_w < 560 - 335:
        x_start = random.randint(335, 560 - text_w)
        draw = ImageDraw.Draw(img)
        draw.text((x_start, y_start), text_level1, font=font, fill=(50, 60, 50, 255))
    else:
        while True:
            rndidx = random.randint(0, len(level1))
            text_level1 = level1[rndidx]
            font = ImageFont.truetype(font_path, font_size)
            text_w, text_h = font.getsize(text_level1)
            if text_w < 560 - 335:
                break
        x_start = random.randint(335, 560 - text_w)
        draw = ImageDraw.Draw(img)
        draw.text((x_start, y_start), text_level1, font=font, fill=(50, 60, 50, 255))
    address1 = [x_start - 2, y_start - 2, x_start + text_w, y_start, x_start + text_w + 2, y_start + text_h, x_start,
                y_start + text_h, text_level1.replace(" ", "")]

    text_lv2andlv3 = text_level2 + ", " + text_level3
    text_w, text_h = font.getsize(text_lv2andlv3)
    y_start = 297
    if text_w < 560 - 210:
        x_start = random.randint(210, 560 - text_w)
        draw = ImageDraw.Draw(img)
        draw.text((x_start, y_start), text_lv2andlv3, font=font, fill=(50, 60, 50, 255))
    else:
        while True:
            rndidx = random.randint(0, len(level1))
            text_level2 = level2[rndidx]
            text_level3 = level3[rndidx]
            text_lv2andlv3 = text_level2 + ", " + text_level3
            font = ImageFont.truetype(font_path, font_size)
            text_w, text_h = font.getsize(text_lv2andlv3)
            if text_w < 560 - 210:
                break
        x_start = random.randint(210, 560 - text_w)
        draw = ImageDraw.Draw(img)
        draw.text((x_start, y_start), text_lv2andlv3, font=font, fill=(50, 60, 50, 255))
    address2 = [x_start - 2, y_start - 2, x_start + text_w, y_start, x_start + text_w + 2, y_start + text_h, x_start,
                y_start + text_h, text_lv2andlv3.replace(" ", "")]
    y_start = 330
    text_w, text_h = font.getsize(text_level1)
    if text_w < 560 - 400:
        x_start = random.randint(400, 560 - text_w)
        draw = ImageDraw.Draw(img)
        draw.text((x_start, y_start), text_level1, font=font, fill=(50, 60, 50, 255))
    else:
        while True:
            rndidx = random.randint(0, len(level1))
            text_level1 = level1[rndidx]
            font = ImageFont.truetype(font_path, font_size)
            text_w, text_h = font.getsize(text_level1)
            if text_w < 560 - 400:
                break
        x_start = random.randint(400, 560 - text_w)
        draw = ImageDraw.Draw(img)
        draw.text((x_start, y_start), text_level1, font=font, fill=(50, 60, 50, 255))
    address3 = [x_start - 2, y_start - 2, x_start + text_w, y_start, x_start + text_w + 2, y_start + text_h, x_start,
                y_start + text_h, text_level1.replace(" ", "")]
    text_w, text_h = font.getsize(text_lv2andlv3)
    y_start = 362
    if text_w < 560 - 210:
        x_start = random.randint(210, 560 - text_w)
        draw = ImageDraw.Draw(img)
        draw.text((x_start, y_start), text_lv2andlv3, font=font, fill=(50, 60, 50, 255))
    else:
        while True:
            rndidx = random.randint(0, len(level1))
            text_level2 = level2[rndidx]
            text_level3 = level3[rndidx]
            font = ImageFont.truetype(font_path, font_size)
            text_w, text_h = font.getsize(text_level1)
            if text_w < 560 - 200:
                break
        text_lv2andlv3 = text_level2 + ", " + text_level3
        x_start = random.randint(210, 560 - text_w)
        draw = ImageDraw.Draw(img)
        draw.text((x_start, y_start), text_lv2andlv3, font=font, fill=(50, 60, 50, 255))
    address4 = [x_start - 2, y_start - 2, x_start + text_w, y_start, x_start + text_w + 2, y_start + text_h, x_start,
                y_start + text_h, text_lv2andlv3.replace(" ", "")]
    all_address = [address1, address2, address3, address4]
    return img, all_address


def write2file(path_outputs, number_img=1000, path_font=None, font_size=32, path_names=None):
    path_texts = os.path.join(path_outputs, "gt_location")
    path_imgs = os.path.join(path_outputs, "imgs")

    for iter in range(0, number_img):
        path_text = os.path.join(path_texts, "gen_{}.txt".format(iter))
        path_img = os.path.join(path_imgs, "gen_{}.jpg".format(iter))
        file = open(path_text, "w")
        img_patter = Image.open('/home/quyetna/Desktop/template_cmt.jpg')
        img, box_name, box_number, box_dob, all_address = draw_name(img=img_patter, font_path=path_font,
                                                                    font_size=font_size, path_names=path_names)
        for index, content in enumerate(box_name):
            box_name[index] = str(content)
        full_name = ",".join(box_name)

        for index, content in enumerate(box_number):
            box_number[index] = str(content)
        text_numer = ",".join(box_number)
        for index, content in enumerate(box_dob):
            box_dob[index] = str(content)
        text_dob = ",".join(box_dob)

        address1, address2, address3, address4 = all_address

        for index, content in enumerate(address1):
            address1[index] = str(content)
        text_address1 = ",".join(address1)

        for index, content in enumerate(address2):
            address2[index] = str(content)
        text_address2 = ",".join(address2)

        for index, content in enumerate(address3):
            address3[index] = str(content)
        text_address3 = ",".join(address3)

        for index, content in enumerate(address4):
            address4[index] = str(content)
        text_address4 = ",".join(address4)

        for index, content in enumerate(CHCNVN):
            CHCNVN[index] = str(content)
        CHCNVN_text = ",".join(CHCNVN)

        for index, content in enumerate(DL):
            DL[index] = str(content)
        DL_text = ",".join(DL)

        for index, content in enumerate(TD):
            TD[index] = str(content)
        TD_text = ",".join(TD)

        for index, content in enumerate(HP):
            HP[index] = str(content)
        HP_text = ",".join(HP)

        for index, content in enumerate(GCMNN):
            GCMNN[index] = str(content)
        GCMNN_text = ",".join(GCMNN)

        for index, content in enumerate(SO):
            SO[index] = str(content)
        SO_text = ",".join(SO)

        for index, content in enumerate(HT):
            HT[index] = str(content)
        HT_text = ",".join(HT)

        for index, content in enumerate(NS):
            NS[index] = str(content)
        NS_text = ",".join(NS)

        for index, content in enumerate(NQ):
            NQ[index] = str(content)
        NQ_text = ",".join(NQ)

        for index, content in enumerate(DKTT):
            DKTT[index] = str(content)
        DKTT_text = ",".join(DKTT)

        file.write(CHCNVN_text + "\n")
        file.write(DL_text + "\n")
        file.write(TD_text + "\n")
        file.write(HP_text + "\n")
        file.write(GCMNN_text + "\n")
        file.write(SO_text + "\n")
        file.write(HT_text + "\n")
        file.write(NS_text + "\n")
        file.write(NQ_text + "\n")
        file.write(DKTT_text + "\n")
        file.write(text_numer + "\n")
        file.write(full_name + "\n")
        file.write(text_dob + "\n")
        file.write(text_address1 + "\n")
        file.write(text_address2)
        file.write(text_address3 + "\n")
        file.write(text_address4)
        file.close()
        img = np.asarray(img)
        cv2.imwrite(path_img, img)


img = Image.open('/home/quyetna/Desktop/template_cmt.jpg')
path_names = "/home/quyetna/PycharmProjects/gen_cmt/names/"
path_patter = '/home/quyetna/Desktop/template_cmt.jpg'
path_font = '/home/quyetna/PycharmProjects/gen_cmt/fonts/Palatino Linotype Regular.ttf'
path_outputs = "/home/quyetna/PycharmProjects/gen_cmt/outputs"

# draw_name(img, path_font, font_size=32, path_names=path_names)
write2file(path_outputs=path_outputs, path_font=path_font, path_names=path_names)
