import os
import xlwt
import xlrd

def txt2excel(folder_path):
    files = os.listdir(folder_path)
    workbook = xlwt.Workbook(encoding='ascii')
    worksheet = workbook.add_sheet('ground_truth', cell_overwrite_ok=True)

    for index, file in enumerate(sorted(files)):
        file_txt = os.path.join(folder_path, file)
        worksheet.write(index, 0, file)
        with open(file_txt, encoding='utf-8') as fi:
            address_1 = ""
            address_2 = ""
            for line in fi:
                line = line.strip('\n')
                if line:
                    list_ = line.split("\t")
                    if len(list_) == 2:
                        number = int(list_[1])
                        list_word = list_[0].split(",")
                        context = list_word[8:len(list_word)]
                        context = "".join(context)
                        if number == 1:
                            worksheet.write(index, 1, context)
                        elif number == 2:
                            worksheet.write(index, 2, context)
                        elif number == 5:
                            worksheet.write(index, 3, str(context))
                        elif number == 6:
                            address_1 += context
                            worksheet.write(index, 4, address_1)
                        elif number == 7:
                            address_2 += context
                            worksheet.write(index, 5, address_2)

    workbook.save('ground_truth.xls')


txt2excel("/media/quyetna/C49F-BDDA/nam80/nam/done/")

def auto_test(xsl_predict, xsl_ground_truth):
    wb_gt = xlrd.open_workbook(xsl_ground_truth)
    wb_pred = xlrd.open_workbook(xsl_predict)
    sheet_gt = wb_gt.sheet_by_index(0)
    sheet_pred = wb_pred.sheet_by_index(0)
    count_
    for i in range(sheet_gt.nrows):
        if sheet_gt.cell(i,1) == sheet_pred(i,1):

